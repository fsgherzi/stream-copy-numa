CXX = g++
CXXFLAGS += -std=c++23 -O3 -ffast-math -fopenmp
LIBS += -lnuma

SOURCES = main.cpp
TARGET = stream-copy

all: $(TARGET) 

$(TARGET): main.cpp 
	$(CXX) $(LDFLAGS) -o stream-copy $(CXXFLAGS) $(SOURCES) $(LIBS)

clean:
	rm -f $(TARGET) 

install:
	mkdir -p ../bin
	cp -f $(TARGET) ../bin/$(TARGET)

test: clean $(TARGET)
	./stream-copy 50000000 0 0 4 100
