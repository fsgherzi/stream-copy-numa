# Stream Copy NUMA benchmark

## Building
Run `make` to build. Requires libnuma and openmp.

## Running
Run 
```bash
./stream-copy <vector size> <src numa node id> <dest numa node id> <data type size> <iterations>
```


