#include <cassert>
#include <chrono>
#include <iostream>
#include <numa.h>
#include <omp.h>

#include "utils/fstypes.hpp"
#include "utils/fsutils.hpp"

#ifdef ENABLE_PARSEC_HOOKS
#include "hooks.h"
#endif

using namespace fs;

class Benchmark {
public:
  virtual Benchmark *setup() = 0;
  virtual void run(u32 iterations, u32 num_threads) = 0;
};

template <typename T> class BenchmarkStream : public Benchmark {
public:
  BenchmarkStream(u32 vs, u32 sn, u32 dn)
      : Benchmark(), m_vector_size(vs), m_src_numa_node_id(sn),
        m_dest_numa_node_id(dn) {}

  virtual Benchmark *setup() override {
    Timer<std::chrono::nanoseconds> timer("setup");
    m_src =
        (T *)numa_alloc_onnode(m_vector_size * sizeof(T), m_src_numa_node_id);

    if (m_src == nullptr) {
      std::cout << "[ERROR] m_src is nullptr." << std::endl;
      exit(-1);
    }

    m_dest =
        (T *)numa_alloc_onnode(m_vector_size * sizeof(T), m_dest_numa_node_id);

    if (m_dest == nullptr) {
      std::cout << "[ERROR] m_dest is nullptr." << std::endl;
      exit(-1);
    }

    return this;
  }

  virtual void run(u32 iterations, u32 num_threads) override {
    Timer<std::chrono::nanoseconds> timer("run");

#ifdef ENABLE_PARSEC_HOOKS
    __parsec_roi_begin();
#endif
    // ROI:
    for (u32 iter = 0; iter < iterations; ++iter) {

#pragma omp parallel for schedule(static) num_threads(num_threads)
      for (u32 i = 0; i < m_vector_size; ++i) {
        m_dest[i] = m_src[i];
      }
    }

#ifdef ENABLE_PARSEC_HOOKS
    __parsec_roi_end();
#endif
  }

  ~BenchmarkStream() {
    free(m_src);
    free(m_dest);
  }

private:
  T *m_src{nullptr};
  T *m_dest{nullptr};
  u32 m_vector_size{0};
  u32 m_src_numa_node_id{0};
  u32 m_dest_numa_node_id{0};
};

i32 main(i32 argc, c8 **argv) {

  if (argc != 6) {
    std::cout << "[USAGE] ./stream-copy <vector size> <src numa node id> <dest "
                 "numa node id> <data type size> <iterations>"
              << std::endl;
    exit(-1);
  }

  u32 num_threads = std::atoll(std::getenv("OMP_NUM_THREADS"));

#pragma omp parallel num_threads(num_threads)
  { std::cout << "[DEBUG] OpenMP thread spawned." << std::endl; }

  u32 args = 1;
  const u32 vector_size = std::atoll(argv[args++]);
  const u32 src_numa_node_id = std::atoll(argv[args++]);
  const u32 dest_numa_node_id = std::atoll(argv[args++]);
  const u32 data_width = std::atoll(argv[args++]);
  const u32 iterations = std::atoll(argv[args++]);

  const bool has_numa = numa_available() >= 0;

  if (!has_numa) {
    std::cout << "[ERROR] NUMA not available" << std::endl;
    exit(-1);
  }

  const u32 nnuma_nodes = numa_num_configured_nodes();
  if ((src_numa_node_id >= nnuma_nodes) || (dest_numa_node_id >= nnuma_nodes)) {
    std::cout << "[ERROR] either src_numa_node_id(" << src_numa_node_id
              << ") or dest_numa_node_id(" << dest_numa_node_id << ")"
              << " ecceded the number of available NUMA nodes on the system ("
              << nnuma_nodes << ")" << std::endl;
    exit(-1);
  }

  if (data_width != 4 && data_width != 8) {
    std::cout << "[ERROR] Expected data width to be either 4 or 8" << std::endl;
    exit(-1);
  }

  Benchmark *benchmark = nullptr;
  if (data_width == 4)
    benchmark = new BenchmarkStream<f32>(vector_size, src_numa_node_id,
                                         dest_numa_node_id);
  if (data_width == 8)
    benchmark = new BenchmarkStream<f64>(vector_size, src_numa_node_id,
                                         dest_numa_node_id);

  benchmark->setup()->run(iterations, num_threads);
}
